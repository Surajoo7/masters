const fs = require('fs');
const path = require('path');

const dirname = "./country-json/src";

var country_main = require('../Data/country-main.json');

const countrymap = {};
fs.readdir(dirname, function(err, files){
    if(err){
        throw err;
    }
    files.forEach(function(file){
       const json = require(`${dirname}/${file}`); 
       //console.log(json);
       file = file.split('country-by-').join("").split('.json').join("").trim();
       countrymap[file] = json;
    });
   // console.log(countrymap,Object.keys(countrymap).length);

    //fs.writeFile('./country-master.json', JSON.stringify(countrymap), function(){});

   const keys = Object.keys(countrymap);    //keys ==> abbrevation,country,barcode....
       const master = [];
        keys.forEach(function(key){             //data present on each key for eg country: "afgan"abbrevation:"af"..
            const list = countrymap[key];
            list.forEach(function(item){
                master.push(item);
                
            });
        });
        
         //fs.writeFile('./country-123.json', JSON.stringify(master), function(){});

         const masterMap = {};
        master.forEach(function(m){
            if(masterMap[m.country]){
                const exist = masterMap[m.country];
                
                masterMap[m.country] = {
                    ...exist,
                    ...m
                }
                }else{
                masterMap[m.country] = {
                    ...m
                };
            }
        });
        
    //    console.log(masterMap);
         let keyCheck = Object.keys(masterMap);
        const finalMaster = [];
        keyCheck.forEach(function(country){
            finalMaster.push(masterMap[country]);
        });
        //console.log(finalMaster);
       // fs.writeFile('./country-master.json', JSON.stringify(finalMaster), function(){});
    

       //Json File Filteration
       const Filter = [];
        finalMaster.forEach(function(item){
            Filter.push({country:item.country, abbreviation: item.abbreviation,
                religion:item.religion, currency_code:item.currency_code});
        });
        console.log(Filter);
        fs.writeFile('./country-filter.json', JSON.stringify(Filter), function(){});
    });




