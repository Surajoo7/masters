var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json( { 
Status: "success",
message:"Country Master Api Working",
live:{
    
    Home :'http://localhost:3000/api/v1/country',
    List :'http://localhost:3000/api/v1/country/list',
    Lookup:'http://localhost:3000/api/v1/country/lookup',
    region:'http://localhost:3000/api/v1/country/region'}

   });
});

module.exports = router;
