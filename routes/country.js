var express = require('express');
var router = express.Router();
var country_main = require('../Data/country-main.json');

// country list.....................................
var country_list=[];
  country_main.forEach(function(item){
   return{ 
           return :
           country_list.push({
          country : item.country,
          abbreviation : item.abbreviation,
          currency_code: item.currency_code,
          currency_name : item.currency_name,
          continent : item.continent,
          tld : item.tld,
          religion : item.religion,
          iso : item.iso
          })
        }
});

//country lookup using abbreviation.........................

var countrytemp_lookup={};
var country_lookup={};

country_main.forEach(function(item){
  if(item.abbreviation){
    countrytemp_lookup[item.abbreviation]=item;
    country_lookup[item.abbreviation]= {
      country : item.country,
      abbreviation : item.abbreviation,
      currency_code: item.currency_code,
      currency_name : item.currency_name,
      continent : item.continent,
      tld : item.tld,
      religion : item.religion,
      iso : item.iso
    }
  }  
});

//Unique Region...............................

let region=[];

country_main.forEach(function(item){
    region.push(item.continent)
    });
  let unique_region = Array.from(new Set(region));


  // Country by Region................................

  let country_region={};

  country_main.forEach(function(item){
    if(item.continent){ 
            if(country_region[item.continent]){
              country_region[item.continent].push({
            country : item.country,
            abbreviation : item.abbreviation,
            currency_code: item.currency_code,
            currency_name : item.currency_name,
            continent : item.continent,
            tld : item.tld,
            religion : item.religion,
            iso : item.iso
              })
    }
    else{
      country_region[item.continent]=[{
        country : item.country,
        abbreviation : item.abbreviation,
        currency_code: item.currency_code,
        currency_name : item.currency_name,
        continent : item.continent,
        tld : item.tld,
        religion : item.religion,
        iso : item.iso
          }]
    }
  
  }
});

  

//console.log(country_region);









router.get('/', function(req, res, next) {
  res.json( { 
Status: "sucess",
message:"Country Masters API Working",
live:{
    
  List :'http://localhost:3000/api/v1/country/list'}
});
});


router.get('/list', function(req, res, next) {
    res.json( { 
  Status: "success",
  message:"Country List",
  res : country_list
});
});


router.get('/lookup', function(req, res, next) {
  res.json( { 
Status: "success",
message:"Country Lookup",
res : countrytemp_lookup
});
});


router.get('/lookup/:input', function(req, res, next) {
  let input = req.params.input && req.params.input.toString().toUpperCase();
  let data = country_lookup;
  if(input){
    data=country_lookup[input];
  }
  res.json( { 
Status: "success",
message:"Country By Abbrevation or tld",
res : data
});
});


router.get('/region', function(req, res, next) {
  res.json( { 
Status: "success",
message:"Regions",
res : unique_region
});
});

router.get('/region/:search', function(req, res, next) {
  let search = req.params.input && req.params.input.toString();
  let data = country_region;
  if(search){
    data=country_region[search];
  }
  res.json( { 
Status: "success",
message:"Country By Region",
res : data
});
});


module.exports = router;
