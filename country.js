const fs = require('fs');
const path = require('path');

const dirname = "./country-json/src";

const countrymap = {};
fs.readdir(dirname, function(err, files){
    if(err){
        throw err;
    }
    files.forEach(function(file){
       const json = require(`${dirname}/${file}`); 
       //console.log(json);
       file = file.split('country-by-').join("").split('.json').join("").trim();
       countrymap[file] = json;
    });
    //console.log(countrymap, Object.keys(countrymap).length);
    //fs.writeFile('./country-master.json', JSON.stringify(countrymap), function(){});
    const keys = Object.keys(countrymap);
    const master = [];
    keys.forEach(function(key){
        const list = countrymap[key];
        list.forEach(function(item){
            master.push(item);
        });
    });
    const masterMap = {};
    master.forEach(function(m){
        if(masterMap[m.country]){
            const exist = masterMap[m.country];
            masterMap[m.country] = {
                ...exist,
                ...m
            }
        }else{
            masterMap[m.country] = {
                ...m
            };
        }
    });
    let keyCheck = Object.keys(masterMap);
    const finalMaster = [];
    keyCheck.forEach(function(country){
        finalMaster.push(masterMap[country]);
    });
    console.log(finalMaster);
    fs.writeFile('./country-master.json', JSON.stringify(finalMaster), function(){});

});


/*

{
    "key1" : [
        {
            name: "Samal",
            exp: "11"
        }
    ],
    "key2": [
        {
            name: "Samal",
            location: "Mumbai"
        }
    ]
}


[
    {
        name: "Samal",
        exp:11,
        location: "Mumbai"
    }
]

*/

/*
 "country", "country_code", "height", "barcode", "calling_code", "city", "continent", "costline", "currency_code", "currency_name", "tld", "elevation", "flag_base64", "north", "south", "west", "east", "government", "independence", "iso", "landlocked", "languages", "expectancy", "dish", "symbol", "density", "population", "location", "religion", "area", "temperature",
*/